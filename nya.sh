#!/bin/zsh


   # Copyright 2023 "Laura Aléanor"

   # Licensed under the Apache License, Version 2.0 (the "License");
   # you may not use this file except in compliance with the License.
   # You may obtain a copy of the License at

   #     http://www.apache.org/licenses/LICENSE-2.0

   # Unless required by applicable law or agreed to in writing, software
   # distributed under the License is distributed on an "AS IS" BASIS,
   # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   # See the License for the specific language governing permissions and
   # limitations under the License.

binary=$( readlink -f miaowe )

# To debug, uncomment and move >&p to end of line.
coproc nc irc.libera.chat 6667 # |tee /dev/stderr
(cd $2 && exec $binary $1) <&p >&p # |tee /dev/stderr
pkill nc
