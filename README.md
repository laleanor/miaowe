# Miaowe
Miaowe is an IRC bot that prints out image files (PPM at the moment) using mIRC colour codes on request.

Dependencies:
* GHC

To build:

    make

The resulting executable will be called "miaowe".

Miaowe includes no networking code of its own, but assumes stdin and stdout will be connected to an IRC server. A zsh script, nya.sh, is included to help with this. Users of other shells may need to adjust the script to work with their shell. Image files are read from the working directory. If nya.sh is used, image_directory becomes miaowe's PWD.

Miaowe expects to find a configuration file with the name "conf" in its PWD's parent directory. This should be in a key-value format, one entry per line. Strings should not be quoted; anything after the first space is considered part of the value. It should include the following keys: "nick", "username", "realname", "credentials". "Credentials" should be the NickServ password, or username and password separated by a space.

Available commands:
* List -- lists all files in PWD
* Code -- shows code URL
* Help -- helps

Miaowe can optionally include a fortune cookie command. To enable this, compile with: `make GHCFLAGS=-DFORTUNE`. The command will be called "Fortune".

To run:

    ./miaowe <channel>

or:

    ./nya.sh <channel> <image_directory>

Commands are used like this: `miaowe: help` or this: `miaowe, help`.