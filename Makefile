SRCS := miaowe.hs
TARGET := miaowe

$(TARGET): $(SRCS)
	ghc -g $(GHCFLAGS) -o $(TARGET) $(SRCS)
